from django.db import models

class Account(models.Model):
    account_num = models.CharField(max_length=16)
    pin = models.CharField(max_length=32)
    block_flag = models.BooleanField()
    balance = models.FloatField()

class AccessLog(models.Model):
    RESULT_CODES = [ (0,'Access Granted'),\
            (1,'Access Denied'),(2,'Access Blocked')]
    account = models.ForeignKey(Account,on_delete=models.CASCADE)
    result = models.IntegerField(choices = RESULT_CODES)
    timestamp = models.DateTimeField(auto_now_add=True)

class OperationLog(models.Model):
    account = models.ForeignKey(Account,on_delete=models.CASCADE)
    withdraw_summ = models.FloatField()
    timestamp = models.DateTimeField(auto_now_add=True)
    opcode = models.CharField(max_length=2)

