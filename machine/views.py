import hashlib
from django.shortcuts import render,redirect
from machine.models import Account, AccessLog, OperationLog
from machine.forms import AccountLogin,PinForm,AmmountForm
from django.views.decorators.csrf import csrf_protect
from machine.utils import account_required,pin_required

@csrf_protect
def index(request):
    val = AccountLogin(request.POST)
    if request.method == 'POST':
        if val.is_valid():
            acc = Account.objects.filter(\
                    account_num=val.cleaned_data['account_num']).count()
            if acc>0:
                acc_inst = Account.objects.get(\
                    account_num=val.cleaned_data['account_num'])

                if acc_inst.block_flag:
                    access = AccessLog(account=acc_inst,result=2)
                    access.save()
                    return render(request,'machine/error.html')
                else:
                    request.session['acc']=val.cleaned_data['account_num']
                    return redirect('machine:pincode')
            else:
                return render(request,'machine/error.html')
        else:
            return render(request,'machine/error.html')
    return render(request,'machine/index.html',{'form':val})
    
@csrf_protect
@account_required
def pin(request):
    val = PinForm(request.POST)
    if request.method == 'POST':
        if val.is_valid():
            acc = Account.objects.get(\
                    account_num=request.session['acc'])
            if acc.block_flag:
                access = AccessLog(account=acc,result=2)
                access.save()
                return render(request,'machine/error.html')

            digest = hashlib.md5()
            digest.update(val.cleaned_data['pin'].encode('UTF-8'))
            tm=digest.hexdigest()

            if acc.pin == tm:
                access = AccessLog(account=acc,result=0)
                access.save()
                request.session['acc_pin']=acc.pin

                return redirect('machine:main')
            else:
                access = AccessLog(account=acc,result=1)
                access.save()
                access_log = AccessLog.objects.filter(account=acc)[::-1][:4]
                if all(filter(lambda x: x.result in [1,2],access_log)):
                    acc.block_flag=True
                    acc.save()
                    access = AccessLog(account=acc,result=2)
                    access.save()
                    return render(request,'machine/error.html')
                else:    
                    return render(request,'machine/pin.html',{'form':val})
        else:
            return render(request,'machine/error.html')
 
    return render(request,'machine/pin.html',{'form':val})

@account_required
@pin_required
def main(request):
    return render(request,'machine/main.html')

@account_required
@pin_required
def exit(request):
    del request.session['acc']
    del request.session['acc_pin']
    return redirect('machine:index')

@csrf_protect
@account_required
@pin_required
def withdraw(request):
    val = AmmountForm(request.POST)

    if request.method == "POST":
        if val.is_valid():
            acc = Account.objects.get(account_num=request.session['acc'])
            if 0< val.cleaned_data['amount']<acc.balance:
                acc.balance -= val.cleaned_data['amount']
                acc.save()
                op = OperationLog(account = acc,\
                        withdraw_summ =val.cleaned_data['amount'],opcode='wd')
                op.save()
                return render(request, 'machine/success.html')
            else:
                return render(request,'machine/error.html')

        else:
            return render(request,'machine/error.html')


    return render(request,'machine/withdraw.html',{'form':val})

    

@account_required
@pin_required
def balance(request):
    acc = Account.objects.get(account_num=request.session['acc'])
    op = OperationLog(account = acc,withdraw_summ=0,opcode='bl')
    op.save()
    return render(request,'machine/status.html',{'result':acc})
