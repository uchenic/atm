from django import forms

from machine.models import Account

class AccountLogin(forms.ModelForm):
    class Meta:
        model = Account
        fields = ['account_num']

class PinForm(forms.Form):
    pin = forms.CharField(max_length=4)

class AmmountForm(forms.Form):
    amount = forms.IntegerField()
