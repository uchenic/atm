from django.shortcuts import render

def account_required(f):
    def wrapper(request):
        if 'acc' in request.session:
            f(request)
        else:
            return render(request,'machine/error.html')
    return wrapper

def pin_required(f):
    def wrapper(request):
        if 'acc_pin' in request.session:
            f(request)
        else:
            return render(request,'machine/error.html')
    return wrapper


