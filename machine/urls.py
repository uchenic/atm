from django.conf.urls import url
from . import views

urlpatterns = [
        url(r'^pincode$',views.pin,name='pincode'),
        url(r'^main$',views.main,name='main'),
        url(r'^exit$',views.exit,name='exit'),
        url(r'^balance$',views.balance,name='balance'),
        url(r'^withdraw$',views.withdraw,name='withdraw'),
        url(r'^$',views.index,name='index')
        
        ]
